#! /usr/bin/env python3


from icalendar import Calendar, Event
from collections import OrderedDict
import pygame
from pygame import FULLSCREEN
import time
import datetime
import threading
from http.server import HTTPServer, BaseHTTPRequestHandler
import base64
import logging
logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s")
logger = logging.getLogger("countdown")

FOREGROUND = (255, 255, 255)
FOREGROUND2 = (128, 128, 128)

# L=82% - for 5 seconds, do a visible flash
FOREGROUNDFLASH = (209, 209, 209)
REDFOREGROUNDFLASH = (255, 163, 163)

# L=95% - then, a more discreet flash
FOREGROUNDFLASHLIGHT = (242, 242, 242)
REDFOREGROUNDLIGHT = (255, 229, 229)
BACKGROUND = (0, 0, 0)

current_entry = 0
entries = []


class SrvHandler(BaseHTTPRequestHandler):
    _password = None

    def do_POST(self):
        if self.headers.get("Authorization", "") != SrvHandler._password:
            self.send_response(401)
            self.send_header("WWW-Authenticate", 'Basic realm="Countdown"')
            self.send_header("Content-type", "text/html")
            self.end_headers()
            return
        if self.path.startswith("/start"):
            logger.info("HTTP Start")
            pygame.event.post(pygame.event.Event(pygame.KEYDOWN, key=ord("s")))
        elif self.path.startswith("/pause"):
            logger.info("HTTP Pause")
            pygame.event.post(pygame.event.Event(pygame.KEYDOWN, key=ord("d")))
        elif self.path.startswith("/reset"):
            logger.info("HTTP Reset")
            pygame.event.post(pygame.event.Event(pygame.KEYDOWN, key=ord("r")))
        elif self.path.startswith("/quit"):
            logger.info("HTTP Quit")
            pygame.event.post(pygame.event.Event(pygame.KEYDOWN, key=ord("q")))
        elif self.path.startswith("/set/3"):
            logger.info("HTTP Reset 3 minutes")
            pygame.event.post(pygame.event.Event(pygame.KEYDOWN, key=pygame.K_1))
        elif self.path.startswith("/set/13"):
            logger.info("HTTP Reset 13 minutes")
            pygame.event.post(pygame.event.Event(pygame.KEYDOWN, key=pygame.K_2))
        elif self.path.startswith("/set/25"):
            logger.info("HTTP Reset 25 minutes")
            pygame.event.post(pygame.event.Event(pygame.KEYDOWN, key=pygame.K_3))
        elif self.path.startswith("/set/40"):
            logger.info("HTTP Reset 40 minutes")
            pygame.event.post(pygame.event.Event(pygame.KEYDOWN, key=pygame.K_4))
        elif self.path.startswith("/set/55"):
            logger.info("HTTP Reset 55 minutes")
            pygame.event.post(pygame.event.Event(pygame.KEYDOWN, key=pygame.K_5))
        elif self.path.startswith("/countdown/7"):
            logger.info("Pre-start countdown")
            pygame.event.post(pygame.event.Event(pygame.KEYDOWN, key=ord("d")))
            pygame.event.post(pygame.event.Event(pygame.KEYDOWN, key=pygame.K_9))
        else:
            logger.info("Invalid request path", self.path)
            self.send_response(400)
            self.end_headers()
            return

        self.send_response(200)
        self.end_headers()


def run_http_server(timer, password):
    server_address = ("0.0.0.0", 9000)
    logger.info(f"Listening on {server_address}")
    logger.info(f"Use HTTP basic auth, user:{password}")
    httpd = HTTPServer(server_address, SrvHandler)
    SrvHandler._password = (
        b"Basic " + base64.b64encode(b"user:" + password.encode("utf-8"))
    ).decode("utf-8")
    while True:
        httpd.handle_request()


def find_entry(entries, now):
    for idx, entry in enumerate(entries):
        if now > entry["start"]:
            return idx
    return len(entries) - 1


class Timer:
    def __init__(self, options):
        self.options = options
        opt = pygame.HWSURFACE | pygame.DOUBLEBUF
        self.fullscreen = False
        if self.options.fullscreen:
            self.fullscreen = True
            opt |= FULLSCREEN
        pygame.init()
        self.scr = pygame.display.set_mode((1920, 1080), opt)
        self.font = pygame.font.Font(pygame.font.get_default_font(), 700)

    def toggle_fullscreen(self):
        screen = self.scr
        tmp = screen.convert()

        w, h = screen.get_width(), screen.get_height()
        flags = screen.get_flags()
        bits = screen.get_bitsize()

        pygame.display.quit()
        pygame.display.init()

        if self.fullscreen:
            logger.info("switch to windowed")
            flags = pygame.HWSURFACE | pygame.DOUBLEBUF
        else:
            logger.info("switch to FS")
            flags = pygame.HWSURFACE | pygame.DOUBLEBUF | FULLSCREEN
        self.fullscreen = not self.fullscreen

        screen = pygame.display.set_mode((w, h), flags, bits)
        screen.blit(tmp, (0, 0))

        pygame.key.set_mods(0)  # HACK: work-a-round for a SDL bug??
        self.scr = screen

    def write(self, txt, col=None):
        if col is None:
            col = FOREGROUND
        t = self.font.render(txt, True, col)
        self.scr.fill(BACKGROUND)
        self.scr.blit(t, (50, 220))
        pygame.display.flip()

    #        pygame.display.update()

    def write_time(self, t, col=None):
        self.write("%02i:%02i" % (t / 60, t % 60), col=col)

    def handle_inc_dec(self, key):
        if key in (
            pygame.K_PLUS,
            pygame.K_KP_PLUS,
            pygame.K_UP,
            pygame.K_PAGEUP,
            pygame.K_EQUALS,
        ):
            logger.info("+1mn")
            return 60
        else:
            logger.info("-1mn")
            return -60

    def run(self):
        global entries
        global current_entry
        while True:
            self.write_time(self.init_time)
            state = "STOPPED"
            tremain = self.init_time
            tfin = 0
            while True:
                for event in pygame.event.get():
                    if (
                        event.type == pygame.QUIT
                        or event.type == pygame.KEYDOWN
                        and event.key in (0x1B, ord("q"))
                    ):
                        # q, ESC
                        state = "QUIT"
                        break
                    elif event.type == pygame.KEYDOWN:
                        if event.key in (pygame.K_RSHIFT, pygame.K_LSHIFT, 0x400000e1, 0x400000e5):
                            # shift
                            continue
                        elif event.key == ord("r"):
                            state = "RESET"
                            break
                        elif event.key == ord("f"):
                            self.toggle_fullscreen()
                            break
                        elif state == "STOPPED":
                            # numpad + / - / up / down
                            logger.debug(event.key)
                            if event.key in (
                                pygame.K_PLUS,
                                pygame.K_MINUS,
                                pygame.K_KP_PLUS,
                                pygame.K_KP_MINUS,
                                pygame.K_UP,
                                pygame.K_DOWN,
                                pygame.K_PAGEUP,
                                pygame.K_PAGEDOWN,
                                pygame.K_EQUALS,  # same as "+" on AZERTY keyboard
                                pygame.K_6,  # same as "-" on AZERTY keyboard
                            ):
                                tremain += self.handle_inc_dec(event.key)
                            elif event.key in (pygame.K_1, pygame.K_KP1, pygame.K_AMPERSAND):
                                tremain = 3*60
                                tfin = 0
                            elif event.key in (pygame.K_2, pygame.K_KP2, 233):
                                tremain = 13*60
                                tfin = 0
                            elif event.key in (pygame.K_3, pygame.K_KP3, pygame.K_QUOTEDBL):
                                tremain = 25*60
                                tfin = 0
                            elif event.key in (pygame.K_4, pygame.K_KP4, pygame.K_QUOTE):
                                tremain = 40*60
                                tfin = 0
                            elif event.key in (pygame.K_5, pygame.K_KP5, pygame.K_LEFTPAREN):
                                tremain = 55*60
                                tfin = 0
                            elif event.key in (pygame.K_9, pygame.K_KP9, 231):
                                state = "PRESTART"
                                tfin = time.time() + 7
                            elif event.key == ord("p"):
                                if current_entry > 0:
                                    current_entry -= 1
                                    logger.info(
                                        "Now at : %s" % entries[current_entry]["title"]
                                    )
                                    tremain = entries[current_entry]["duration"]
                                else:
                                    logger.info("Already at first entry")
                            elif event.key == ord("n"):
                                if current_entry < len(entries) - 1:
                                    current_entry += 1
                                    logger.info(
                                        "Now at : %s" % entries[current_entry]["title"]
                                    )
                                    tremain = entries[current_entry]["duration"]
                                else:
                                    logger.info("Already at last entry")
                            elif event.key == ord("d"):
                                # do nothing: "d" switches to STOPPED state
                                pass
                            else:
                                state = "STARTED"
                                tfin = time.time() + tremain
                        elif state == "STARTED":
                            logger.debug(event.key)
                            if event.key in (
                                pygame.K_PLUS,
                                pygame.K_MINUS,
                                pygame.K_KP_PLUS,
                                pygame.K_KP_MINUS,
                                pygame.K_UP,
                                pygame.K_DOWN,
                                pygame.K_PAGEUP,
                                pygame.K_PAGEDOWN,
                                pygame.K_EQUALS,  # same as "+" on AZERTY keyboard
                                pygame.K_6,  # same as "-" on AZERTY keyboard
                            ):
                                if tfin < time.time():
                                    # useful for increases
                                    tfin = time.time()
                                tfin += self.handle_inc_dec(event.key)
                                if tfin < time.time():
                                    # useful for decreases
                                    tfin = time.time()
                            elif event.key == ord("s"):
                                # do nothing: "s" switches to STARTED state
                                pass
                            elif event.key in (pygame.K_1, pygame.K_KP1, pygame.K_AMPERSAND):
                                tremain = 3*60
                                tfin = 0
                                state = "STOPPED"
                            elif event.key in (pygame.K_2, pygame.K_KP2, 233):
                                tremain = 13*60
                                tfin = 0
                                state = "STOPPED"
                            elif event.key in (pygame.K_3, pygame.K_KP3, pygame.K_QUOTEDBL):
                                tremain = 25*60
                                tfin = 0
                                state = "STOPPED"
                            elif event.key in (pygame.K_4, pygame.K_KP4, pygame.K_QUOTE):
                                tremain = 40*60
                                tfin = 0
                                state = "STOPPED"
                            elif event.key in (pygame.K_5, pygame.K_KP5, pygame.K_LEFTPAREN):
                                tremain = 55*60
                                tfin = 0
                                state = "STOPPED"
                            elif event.key in (pygame.K_9, pygame.K_KP9, 231):
                                state = "PRESTART"
                                tremain = tfin - time.time()
                                tfin = time.time() + 7
                            else:
                                if tfin < time.time():
                                    state = "RESET"
                                    break
                                else:
                                    tremain = tfin - time.time()
                                    state = "STOPPED"
                if state == "QUIT":
                    break
                elif state == "RESET":
                    break
                elif state == "STARTED":
                    if tfin >= time.time():
                        self.write_time(tfin - time.time())
                    else:
                        if time.time() - tfin < 5:
                            blink_color = REDFOREGROUNDFLASH
                            foreground = FOREGROUNDFLASH
                        else:
                            blink_color = REDFOREGROUNDLIGHT
                            foreground = FOREGROUNDFLASHLIGHT
                        self.write_time(
                            0,
                            col=[foreground, blink_color][
                                int((tfin - time.time()) * 2.5) % 2
                            ],
                        )
                elif state == "STOPPED":
                    self.write_time(
                        tremain,
                        col=[FOREGROUND, FOREGROUND2][
                            int((tfin - time.time()) * 1.5) % 2
                        ],
                    )
                elif state == "PRESTART":
                    remain = tfin - time.time()
                    if remain < 0:
                        state = "STARTED"
                        tfin = time.time() + tremain
                        continue
                    if remain < 1:
                        value = "GO!"
                    else:
                        value = str(int(tfin - time.time())) + "..."
                    self.write(value)

                pygame.time.delay(25)
            if state == "QUIT":
                break


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser(
        epilog="Keys: [Esc] = exit , [r] = reset\n[+/-] = add/remove 1mn, [p/n] = prev/next slot, [f] = toggle fullscreen, [1-5] = set duration, [9] = countdown, [any other key] = start/stop"
    )
    parser.add_argument(
        "--timer",
        "-t",
        default=300,
        type=int,
        help="Start countdown at SECONDS",
        metavar="SECONDS",
    )
    parser.add_argument(
        "--fullscreen", "--fs", "-F", action="store_true", help="Fullscreen"
    )
    parser.add_argument("--calendar", help="ICS file")
    parser.add_argument("-l", "--log", help="Path to output logs")
    parser.add_argument(
        "-p",
        "--password",
        help="Run HTTP server on port 8000, with Basic Auth, user=user, password=this option",
    )

    options = parser.parse_args()

    if options.calendar:
        global entries
        cal = Calendar.from_ical(open(options.calendar).read())
        for ev in cal.walk("VEVENT"):
            duration = (ev["dtend"].dt - ev["dtstart"].dt).seconds
            # Remove "question's" duration
            if duration == 15 * 60:
                duration -= 2 * 60
            elif duration == 30 * 60:
                duration -= 5 * 60
            entries.append(
                {
                    "start": ev["dtstart"].dt,
                    "duration": duration,
                    "title": ev["SUMMARY"],
                }
            )
        # We're after the start of the conference
        now = datetime.datetime.now()
        if now > entries[0]["start"]:
            global current_entry
            current_entry = find_entry(entries, now)
        logger.info("Current slot: " + entries[current_entry]["title"])

    t = Timer(options)

    if options.log:
        file_handler = logging.FileHandler(options.log)
        file_handler.setLevel(logging.INFO)
        file_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(message)s"))
        logger.addHandler(file_handler)

    if options.password:
        http_thread = threading.Thread(
            target=run_http_server, args=[t, options.password], daemon=True
        )
        http_thread.start()

    try:
        if len(entries) > 0:
            t.init_time = entries[current_entry]["duration"]
        else:
            t.init_time = options.timer
        t.run()
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()
