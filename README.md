## Dependencies

```
apt install python3-icalendar python3-pygame
```

### Usage

```
usage: countdown.py [-h] [--timer SECONDS] [--fullscreen] [--calendar CALENDAR] [-l LOG] [-p PASSWORD]

options:
  -h, --help            show this help message and exit
  --timer SECONDS, -t SECONDS
                        Start countdown at SECONDS
  --fullscreen, --fs, -F
                        Fullscreen
  --calendar CALENDAR   ICS file
  -l LOG, --log LOG     Path to output logs
  -p PASSWORD, --password PASSWORD
                        Run HTTP server on port 8000, with Basic Auth, user=user, password=this option

Keys: [Esc] = exit , [r] = reset [+/-] = add/remove 1mn, [p/n] = prev/next slot, [f] = toggle fullscreen, [1-5] = set duration, [9] = countdown, [any other key] = start/stop
```

Keys:

* `esc`: quit
* `r`: reset
* `f`: fullscreen
* `n`/`p`: next / previous calendar entry
* `+`/`UP`/`PGUP`: add 1 mn to timer
* `-`/`DOWN`/`PGDOWN`: subtract 1 mn to timer
* `s`/`d`: start/stop counter
* `1` from STOPPED state: set timer to 3 minutes
* `2` from STOPPED state: set timer to 13 minutes
* `3` from STOPPED state: set timer to 25 minutes
* `4` from STOPPED state: set timer to 40 minutes
* `5` from STOPPED state: set timer to 55 minutes
* `9` from STOPPED state: countdown for 7 seconds (6... 5... 4... 3... 2... 1... GO!) then switch to STARTED mode
* any other key: toggle STOPPED/STARTED


When the timer reaches 0, it will blink markedly red for 5 seconds, then blink with a softer red. Pressing the down arrow at that time brings back the more contrasted red for 5 seconds.
